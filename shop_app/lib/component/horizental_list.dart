import 'package:flutter/material.dart';
class HorizentalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Categorey(
            image_location: 'images/cats/tshirt.png',
            image_caption: 'Tshirt',
          ),
          Categorey(
            image_location: 'images/cats/formal.png',
            image_caption: 'formal',
          ),
          Categorey(
            image_location: 'images/cats/dress.png',
            image_caption: 'dress',
          ),

          Categorey(
            image_location: 'images/cats/informal.png',
            image_caption: 'informal',
          ),

          Categorey(
            image_location: 'images/cats/shoe.png',
            image_caption: 'shoes',
          ),



          Categorey(
            image_location: 'images/cats/accessories.png',
            image_caption: 'Tshirt',
          ),
         

        ],
      ),
    );
  }
}
class Categorey extends StatelessWidget {

  final String image_location;
  final String image_caption;
  Categorey({
    this.image_location,
    this.image_caption,
});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: InkWell(
        onTap: (){},
        child: Container(
          width: 100.0,
          child: ListTile(
            title: Image.asset(
                image_location,
              width: 100.0,
              height: 80.0,


            ),
            subtitle: Container(
              alignment: Alignment.center,
                child: Text(
                    image_caption,
                  style: new TextStyle(fontSize: 12.0),

                ),
            ),
          ),
        ),
      ),
    );
  }
}
