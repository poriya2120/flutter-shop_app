import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

//own pakage
import 'package:shopapp/component/horizental_list.dart';
import 'package:shopapp/component/productList.dart';
void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
home: Homepage(),

));
class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    Widget image_carousel=new Container(
      height: 200,
      child: new Carousel(
        boxFit: BoxFit.cover,
        images: [
          AssetImage('images/a.jpg'),
          AssetImage('images/b.jpg'),
          AssetImage('images/c.jpg'),
          AssetImage('images/d.jpg'),
          AssetImage('images/m2.jpg'),

        ],
        autoplay: true,
        animationCurve: Curves.fastOutSlowIn,
         animationDuration: Duration(milliseconds: 1000),
      dotSize: 4.0,
        dotColor: Colors.red,
        indicatorBgPadding: 2.0,
      ),
    );
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        title: Text('ShopApp'),
        backgroundColor: Colors.red,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,

            ),
            onPressed: (){},
          ),
          IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.white,

            ),
            onPressed: (){},
          ),
        ],
      ),
      drawer: Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(

                accountName: Text('Poriya Daliry'),
                accountEmail: Text('poriya2120@gmail.com'),
              currentAccountPicture:GestureDetector(
                child: CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(
                      Icons.person,
                    color: Colors.white,
                  ),
                ),
              ) ,
              decoration: new BoxDecoration(
                color: Colors.red
              ),
            ),

            //body


            InkWell(
                onTap: (){},
                child:ListTile(
                  title: Text("Homepage"),
                  leading: Icon(
                      Icons.home
                  ),
                )
            ),

            InkWell(
                onTap: (){},
                child:ListTile(
                  title: Text("Myacount"),
                  leading: Icon(
                      Icons.person
                  ),
                )
            ),

            InkWell(
                onTap: (){},
                child:ListTile(
                  title: Text("MyOrder"),
                  leading: Icon(
                      Icons.shopping_basket
                  ),
                )
            ),

            InkWell(
                onTap: (){},
                child:ListTile(
                  title: Text("Cateforis"),
                  leading: Icon(
                      Icons.dashboard
                  ),
                )
            ),

            InkWell(
                onTap: (){},
                child:ListTile(
                  title: Text("Favourites"),
                  leading: Icon(
                      Icons.favorite
                  ),
                )
            ),
            Divider(),
            InkWell(
                onTap: (){},
                child:ListTile(
                  title: Text("Setting"),
                  leading: Icon(
                      Icons.settings,
                    color: Colors.blue,
                  ),

                )
            ),
            InkWell(
                onTap: (){},
                child:ListTile(
                  title: Text("About"),
                  leading: Icon(
                      Icons.help,
                    color: Colors.green,
                  ),

                )
            ),
          ],
        ),
      ),
      body: new ListView(
        children: <Widget>[
          image_carousel,
          new Padding(padding: const EdgeInsets.all(8.0),
          child: Text('Categories'),
          ),
          //horizental list
          HorizentalList(),
          new Padding(
            padding: const EdgeInsets.all(8.0),
            child: new Text('recent prodoct'),
          ),
          Container(
            height: 320.0,
            child: productList(),
          )
        ],
      ),
    );
  }
}



